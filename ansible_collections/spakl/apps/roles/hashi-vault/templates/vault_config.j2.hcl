// # Vault configuration template


// listener "tcp" {
//   address     = "0.0.0.0:8200"
//   tls_disable = 1
// }

// ui = true

listener "tcp" {
  address                         = "0.0.0.0:8200"
  cluster_address                 = "0.0.0.0:8201"
  proxy_protocol_behavior         =  "use_always"

  // proxy_protocol_authorized_addrs = [
  
  {% if vault_certs_enabled %}
  
  tls_cert_file                   = "/certs/vault.pem"
  tls_key_file                    = "/certs/vault.key.pem"
  tls_client_ca_file                     = "/certs/ca.pem"
  tls_require_and_verify_client_cert = "false"

  {% endif %}

}
storage "file" {
  path = "/vault/data"
}

// storage "raft" {
//   path = "/vault/data/file"
//   node_id = "{{ container_hostname }}"
// }

api_addr      = "https://{{ vault_domain_name }}"
cluster_addr  = "https://clstr.{{ vault_domain_name }}"
cluster_name  = "container_hostname"
disable_mlock = true
ui            = true
log_level     = "{{ vault_log_level }}"
log_format    = "{{ vault_log_format }}"
log_file      = "{{ vault_container_log_file }}"

