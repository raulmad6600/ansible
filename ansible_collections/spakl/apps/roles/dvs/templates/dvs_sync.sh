#!/bin/bash

# Meta
HOST="{{ dvs_host }}"                                # Name of the docker host for notifications and filename
VERSION="{{ dvs_sync_version }} "                         # You can set this to "latest" or a specific date like "mm-dd-yyyy"

# Minio S3
MINIO_SERVER="{{ dvs_minio_api_url }}"  # Minio api url
BUCKET_NAME="{{ dvs_minio_bucket_name }}"                       # Bucketname
DVS_ACCESS_KEY="{{ dvs_minio_access_key }}"            # minio access key
DVS_SECRET_KEY="{{ dvs_minio_secret_key }}"                      # minio secret key

# Reports
REPORT_ENABLED="{{ dvs_reports_enabled }}"                      # Set to false to disable the report
REPORT_TYPE="{{ dvs_report_type }}"                     # Set to false to disable the report
GOTIFY_URL="{{ dvs_gotify_url }}"      # Gotify base url
GOTIFY_APP_TOKEN="{{ dvs_gotify_token }}"                     # gotify api token
# Function to log messages
DISCORD_WEBHOOK_URL="{{ dvs_discord_webhook_url }}"
ERROR=""              

# Function to log messages
log() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')]: $1"
}

# Validate if mc is installed or not
if ! command -v mc &> /dev/null; then
    log "ERROR: MinIO Client (mc) is not installed."
    ERROR="ERROR: MinIO Client (mc) is not installed."
fi

# Configure MinIO server
mc alias set minio "$MINIO_SERVER" "$DVS_ACCESS_KEY" "$DVS_SECRET_KEY" --api S3v4

# Determine the backup file to restore
if [ "$VERSION" == "latest" ]; then
    backup_filename="${HOST}_volume_backup-latest.tar.gz"
else
    # Validate date format if a specific date is provided
    if ! date -d $VERSION &>/dev/null; then
        log "ERROR: Invalid date format."
        exit 1
    fi
    backup_filename="${HOST}_volume_backup-${VERSION}.tar.gz"
fi 
# Sync the backup from the MinIO bucket
if mc cp "minio/$BUCKET_NAME/$backup_filename" "./"; then
    log "Backup file $backup_filename downloaded successfully."
else
    log "ERROR: Failed to download the backup file $backup_filename."
    ERROR="ERROR: Failed to download the backup file $backup_filename."
fi

# Extract the tar.gz file to restore Docker volumes
if sudo tar -xzvf "./$backup_filename" -C /var/lib/docker; then
    log "Docker volumes restored successfully from $backup_filename."
else
    log "ERROR: Failed to restore Docker volumes."
    ERROR="ERROR: Failed to restore Docker volumes."
fi


# Send notification based on the report type
if [ "$REPORT_ENABLED" = "true" ] ; then
    log "Preparing to send notification..."

    if [ "$REPORT_TYPE" = "discord" ] || [ "$REPORT_TYPE" = "*" ]; then
        # Send notification to Discord
        log "Sending notification to Discord..."
        
        if [[ $ERROR != "" ]]; then
            STATUS="Backup failed"
            MESSAGE="[${HOST}]: Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Error: $ERROR, Status: $STATUS"
        else
            STATUS="Backup was successful"
            MESSAGE="Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS"
        fi
        
        curl -H "Content-Type: application/json" \
        -X POST \
        -d '{"content":"'"$MESSAGE"'"}' \
        $DISCORD_WEBHOOK_URL

        log "Discord notification sent."
    fi

    if [ "$REPORT_TYPE" = "gotify" ] || [ "$REPORT_TYPE" = "*" ]; then
        # Send notification to Gotify
        log "Sending notification to Gotify server..."

        if [[ $ERROR != "" ]]; then
            STATUS="Backup failed"
            MESSAGE="Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Error: $ERROR, Status: $STATUS"
        else
            STATUS="Backup was successful"
            MESSAGE="Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS"
        fi

        curl -k -X POST "$GOTIFY_URL/message?token=$GOTIFY_APP_TOKEN" \
        -F "title=Backup Status for $HOST" \
        -F "message=$MESSAGE" \
        -F "priority=5"

        log "Gotify notification sent."
    fi

    if [ "$REPORT_TYPE" != "gotify" ] && [ "$REPORT_TYPE" != "discord" ] && [ "$REPORT_TYPE" != "*" ]; then
        log "Invalid report type specified. Skipping notification."
    fi

    log "Notification process completed."
fi



# Optionally, clean up the downloaded backup file
if sudo rm "./$backup_filename"; then
    log "Local backup file removed successfully."
else
    log "ERROR: Failed to remove local backup file."
fi

log "Restore process completed."
