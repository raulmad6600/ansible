#!/bin/bash

# Meta
HOST="{{ dvs_host }}"                                # Name of the docker host for notifications and filename

# Minio S3
MINIO_SERVER="{{ dvs_minio_api_url }}"  # Minio api url
BUCKET_NAME="{{ dvs_minio_bucket_name }}"                       # Bucketname
DVS_ACCESS_KEY="{{ dvs_minio_access_key }}"            # minio access key
DVS_SECRET_KEY="{{ dvs_minio_secret_key }}"                      # minio secret key

# Reports
REPORT_ENABLED="{{ dvs_reports_enabled }}"                    # Set to false to disable the report
REPORT_TYPE="{{ dvs_report_type }}"                     # Set to false to disable the report
GOTIFY_URL="{{ dvs_gotify_url }}"      # Gotify base url
GOTIFY_APP_TOKEN="{{ dvs_gotify_token }}"                     # gotify api token
# Function to log messages
DISCORD_WEBHOOK_URL="{{ dvs_discord_webhook_url }}"
ERROR=""

log() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')]: $1"
}

# Validate if the necessary tools are installed or not
if ! command -v tar &> /dev/null || ! command -v curl &> /dev/null; then
    log "ERROR: The required tools (tar, curl) are not installed."
    exit 1
fi

# Get Current Date
current_date=$(date '+%m-%d-%Y')

# Create a tar.gz file of the Docker volumes with the current date appended to the filename
backup_filename="${HOST}_volume_backup-$current_date.tar.gz"
latest_backup_filename="${HOST}_volume_backup-latest.tar.gz"

if sudo tar -czvf "./$backup_filename" -C /var/lib/docker volumes; then
    log "Backup created successfully."
else
    log "ERROR: Failed to create backup."
    exit 1
fi

# Also create a 'latest' backup
sudo cp "./$backup_filename" "./$latest_backup_filename"

# Install MinIO Client if it is not already installed
if ! command -v mc &> /dev/null; then
    log "Installing MinIO Client..."

    ARCH=$(uname -m)
    if [ "$ARCH" = "x86_64" ]; then
        MC_URL="https://dl.min.io/client/mc/release/linux-amd64/mc"
    elif [ "$ARCH" = "armv7l" ]; then
        MC_URL="https://dl.min.io/client/mc/release/linux-arm/mc"
    elif [ "$ARCH" = "aarch64" ]; then
        MC_URL="https://dl.min.io/client/mc/release/linux-arm64/mc"
    else
        log "Unsupported architecture: $ARCH"
        exit 1
    fi

    sudo curl -o /tmp/mc -L $MC_URL
    chmod +x /tmp/mc
    sudo mv /tmp/mc /usr/local/bin
fi


# Configure MinIO server
mc alias set minio "$MINIO_SERVER" "$DVS_ACCESS_KEY" "$DVS_SECRET_KEY" --api S3v4

# Check if the bucket exists
if mc ls minio | grep -q "$BUCKET_NAME"; then
    log "Bucket already exists."
else
    if mc mb "minio/$BUCKET_NAME"; then
        log "Bucket created successfully."
    else
        log "ERROR: Failed to create bucket."
        ERROR="ERROR: Failed to create bucket."
    fi
fi

# Copy the tar.gz file with the current date to the bucket
if mc cp "./$backup_filename" "minio/$BUCKET_NAME"; then
    log "Backup uploaded successfully."
else
    log "ERROR: Failed to upload backup."
    ERROR="ERROR: Failed to upload backup."
fi

# Overwrite the 'latest' backup in the bucket
if mc cp "./$latest_backup_filename" "minio/$BUCKET_NAME"; then
    log "'Latest' backup overwritten successfully."
else
    log "ERROR: Failed to overwrite 'latest' backup."
    ERROR="ERROR: Failed to overwrite 'latest' backup."
fi


# Send notification based on the report type
if [ "$REPORT_ENABLED" = "true" ] ; then
    log "Preparing to send notification..."

    if [ "$REPORT_TYPE" = "discord" ] || [ "$REPORT_TYPE" = "*" ]; then
        # Send notification to Discord
        log "Sending notification to Discord..."
        
        if [[ $ERROR != "" ]]; then
            STATUS="Backup failed"
            MESSAGE="[${HOST}]: Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Error: $ERROR, Status: $STATUS"
        else
            STATUS="Backup was successful"
            MESSAGE="[${HOST}]: Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS"
        fi
        
        curl -H "Content-Type: application/json" \
        -X POST \
        -d '{"content":"'"$MESSAGE"'"}' \
        $DISCORD_WEBHOOK_URL

        log "Discord notification sent."
    fi

    if [ "$REPORT_TYPE" = "gotify" ] || [ "$REPORT_TYPE" = "*" ]; then
        # Send notification to Gotify
        log "Sending notification to Gotify server..."

        if [[ $ERROR != "" ]]; then
            STATUS="Backup failed"
            MESSAGE="Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Error: $ERROR, Status: $STATUS"
        else
            STATUS="Backup was successful"
            MESSAGE="Backup completed on $(date '+%Y-%m-%d %H:%M:%S'). Status: $STATUS"
        fi

        curl -k -X POST "$GOTIFY_URL/message?token=$GOTIFY_APP_TOKEN" \
        -F "title=Backup Status for $HOST" \
        -F "message=$MESSAGE" \
        -F "priority=5"

        log "Gotify notification sent."
    fi

    if [ "$REPORT_TYPE" != "gotify" ] && [ "$REPORT_TYPE" != "discord" ] && [ "$REPORT_TYPE" != "*" ]; then
        log "Invalid report type specified. Skipping notification."
    fi

    log "Notification process completed."
fi


# Clean up local backup files
if sudo rm "./$backup_filename" && sudo rm "./$latest_backup_filename"; then
    log "Local backup files removed successfully."
else
    log "ERROR: Failed to remove local backup files."
fi

log "Backup process completed."
