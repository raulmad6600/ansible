create tsig key
```sh
sudo docker run --rm -it ubuntu/bind9:latest tsig-keygen -a hmac-sha256

```

example output:
```sh
key "tsig-key" {
        algorithm hmac-sha256;
        secret "71TudbXpkjyJrXcn9UYMuNLK0w8CsjpYhaUz219VrZo=";
};
```