# k3s_master Ansible Role

This Ansible role is responsible for setting up the master nodes in a K3s cluster. It ensures that the required K3s version is installed and configures it based on the provided variables.

## Requirements

- Ansible version 2.1 or newer.
- SSH access to the target master nodes.
- `curl` should be installed on the target nodes.
- The server nodes must be able to access `https://get.k3s.io`.

## Role Variables

Variables are loaded from the inventory's `vault.yml` and `group_vars/main-cluster.yml`.

**Main Variables**:
- `k3s_version`: The desired version of K3s to install.
- `bootstrap_node_ip`: IP address of the bootstrap node.
- `kube_vip_ip`: IP address for Kubernetes VIP.
- `kube_vip_sans_names`: List of Subject Alternative Names (SANs) for the K3s cluster.

**Sensitive Data (from `vault.yml`)**:
- `ansible_ssh_private_key`: The private SSH key for the Ansible user.
- `k3s_node_token`: Token used for joining master or worker nodes to the cluster.

## Dependencies

None.

## Example Playbook

```yaml
- name: Setup k3s master node
  hosts: master_nodes
  become: yes
  gather_facts: no
  vars_files:
    - ../inventory/vault.yml
    - ../inventory/group_vars/main-cluster.yml
  roles:
    - roles/k3s_master
```

**Pre-tasks**:

Before the main tasks of the role are executed, there are preparatory tasks that:

- Create a temporary SSH key file on the Ansible controller machine.
- Set the `ansible_ssh_private_key_file` variable to point to this temporary key.

**Post-tasks**:

After the role's tasks, the playbook ensures that:

- The temporary SSH key file is removed from the Ansible controller machine.

## License

GPL-2.0-or-later (or specify your preferred license)

## Author Information

This role was created in 2023 by Brock Henrie, Spakl.

