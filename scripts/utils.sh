#!/usr/bin/env bash
if [[ -z "$ACI_UTILS" ]]; then
    export ACI_UTILS=/scripts
fi

source $ACI_UTILS/logger.sh
source $ACI_UTILS/ansible_vault_ops.sh