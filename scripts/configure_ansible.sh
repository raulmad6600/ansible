#!/usr/bin/env bash
## Move ansible.cfg to home dir 
mkdir -p $HOME/.ansible 
cp ./ansible.cfg /etc/ansible/ansible.cfg
cp ./ansible.cfg $HOME/.ansible/ansible.cfg

mv ./ansible_collections/* /usr/lib/python3/dist-packages/ansible_collections/
# cp -r ./ansible_collections $HOME/.ansible/collections/ansible_collections

rm -rf /tmp