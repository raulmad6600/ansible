#!/usr/bin/env bash

source $ACI_UTILS/logger.sh

function ansible_vault_prepare_passwd() {
  local PASSWORD="${AV_PASSWORD}"
  local PASSWORD_PATH="${AV_PASSWORD_PATH-'/root/.ansible/vault_password'}"

  # Parse command-line options using getopt
  local TEMP=$(getopt -o 'p:o' --long 'password:,passwd-path:' -- "$@")
  eval set -- "$TEMP"
  # Process each option
  while true; do
    case $1 in
      -p|--password)
        PASSWORD=$2
        shift 2
        ;;
      -o|--passwd-path)
        PASSWORD_PATH=$2
        shift 2
        ;;
      --)
        shift
        break
        ;;
    esac
  done
  
  if [[ -z "$PASSWORD" ]]; 
  then
    echo "Error: PASSWORD not set";
    exit 1;
  fi

  # Print the values of the flags
  echo "$PASSWORD" > $PASSWORD_PATH
}

function ansible_vault_cleanup() {
  local PASSWORD_PATH="${AV_PASSWORD_PATH-'/home/ansible/.ansible/vault_password'}"

  # Parse command-line options using getopt
  local TEMP=$(getopt -o 'o' --long 'passwd-path:' -- "$@")
  eval set -- "$TEMP"
  # Process each option
  while true; do
    case $1 in
      -o|--passwd-path)
        PASSWORD_PATH=$2
        shift 2
        ;;
      --)
        shift
        break
        ;;
    esac
  done
  # Print the values of the flags
  rm $PASSWORD_PATH
}