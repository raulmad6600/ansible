#!/bin/bash

domain_name="spakl.io"

function new_ca(){
    consul tls ca create -domain=${domain_name} -common-name=Spakl-io
    
    cp ${domain_name}-agent-ca.pem ${domain_name}-ca.pem
    cp ${domain_name}-agent-ca-key.pem ${domain_name}-ca-key.pem

    rm ${domain_name}-agent-ca.pem
    rm ${domain_name}-agent-ca-key.pem 
}

# create server certs


function new_vault_cert(){
    consul tls cert create -server -ca=${domain_name}-ca.pem -domain=$1 -key=${domain_name}-ca-key.pem

    cp dc1-server-${1}-0.pem generated_vault.pem
    cp dc1-server-${1}-0-key.pem generated_vault.key.pem

    rm dc1-server-${1}-0.pem 
    rm dc1-server-${1}-0-key.pem 
}



new_vault_cert "vault.svc.test.dcv.spakl.io"