#!/bin/bash

# Script to generate the desired configuration file

# Prompt user for inputs
read -p "Enter K3S version (default: v1.28.1-rc2+k3s1): " k3s_version
k3s_version=${k3s_version:-"v1.28.1-rc2+k3s1"}

read -p "Enter bootstrap node IP (default: 10.10.4.60): " bootstrap_node_ip
bootstrap_node_ip=${bootstrap_node_ip:-10.10.4.60}

read -p "Enter kube VIP IP (default: 10.10.4.40): " kube_vip_ip
kube_vip_ip=${kube_vip_ip:-10.10.4.40}

read -p "Enter kube VIP interface (default: eth0): " kube_vip_interface
kube_vip_interface=${kube_vip_interface:-eth0}

# Prompt for SANs
declare -a sans_names
sans_names+=("$kube_vip_ip")
sans_names+=("$bootstrap_node_ip")

while true; do
    read -p "Enter SAN name or 'done' to finish: " san
    if [[ "$san" == "done" ]]; then
        break
    fi
    sans_names+=("$san")
done

# Create or overwrite the configuration file
cat <<EOL > config.yml
k3s_version: "${k3s_version}" # Replace with your desired version
bootstrap_node_ip: ${bootstrap_node_ip}
kube_vip_ip: ${kube_vip_ip}
kube_vip_interface: "${kube_vip_interface}"
kube_vip_sans_names:
EOL

# Append the SANs to the file
for san in "${sans_names[@]}"; do
    echo "  - ${san}" >> config.yml
done

echo "Configuration file 'config.yml' has been generated!"
