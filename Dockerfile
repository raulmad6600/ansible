FROM registry.gitlab.com/spakl/common-utils:v1.0.0 as skutils
FROM ubuntu:mantic
LABEL maintainer="robot@spakl.io"
# Explicitly set the shell to Bash (usually redundant for Ubuntu-based images)
COPY --from=skutils /skutils /skutils



ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /tmp 
COPY ./scripts /scripts 
COPY . /tmp 
RUN apt-get update && \
        apt-get install -y --no-install-recommends \
        jq \
        git \
        software-properties-common \
        openssh-client \
        ansible \
        && addgroup --gid 2000 ansible \
        && adduser --uid 2000 --gid 2000 --disabled-password --gecos "" ansible \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/* \
        && mkdir -p /ansible \
        && chmod -R +x /scripts \
        && /scripts/configure_ansible.sh 

RUN ["/bin/bash", "-c", "source /skutils/index.sh && /skutils/scripts/tls/install_ca.sh"]

WORKDIR /ansible
SHELL ["/bin/bash", "-c"]
CMD ["ansible", "--version"]