# Ubuntu Maintenance & Cluster Configuration Guide

## Table of Contents

1. [Ansible Vault and SSH Configuration](#ansible-vault-and-ssh-configuration)
   - [Ansible Vault](#ansible-vault)
   - [Ansible SSH Key Setup](#ansible-ssh-key-setup)
2. [Inventory](#inventory)
   - [Global Variables](#global-variables)
   - [Groups](#groups)
   - [Adding a New Group](#adding-a-new-group-children-section)
3. [Cluster Configuration: `main-cluster.yml`](#cluster-configuration-main-clusteryml)
   - [Variables](#variables)
   - [Adjusting Configuration](#adjusting-configuration)
4. [Role READMEs](#role-readmes)

---

# Ansible Vault and SSH Configuration

## Ansible Vault

Ansible Vault allows for the encryption of sensitive information within our playbooks or variables, ensuring that secrets are not plainly visible.

**Encrypting `vault.yml`:**

```bash
ansible-vault encrypt inventory/vault.yml
```

**Decrypting `vault.yml`:**

```bash
ansible-vault decrypt inventory/vault.yml
```

To avoid entering the password every time you encrypt/decrypt, you can store the vault password in a file and reference it or set it as an environment variable.

**Encrypting with password file:**

```bash
ansible-vault encrypt inventory/vault.yml --vault-password-file /path/to/vault_password.txt
```

**GitLab CI Configuration:**
In your CI/CD setup within GitLab, you can use GitLab's CI/CD environment variables feature to securely store the Ansible Vault password:

1. Navigate to your project in GitLab.
2. Go to `Settings` -> `CI/CD`.
3. Under `Variables`, click `Add Variable`.
4. Name the variable `VAULT_PASSWORD`.
5. Set the variable's value to your Ansible Vault password.
6. Ensure the variable is protected and masked if desired.

For utilizing the `VAULT_PASSWORD` in your GitLab CI pipeline:

```yaml
before_script:
  - echo "$VAULT_PASSWORD" > vault_password.txt
script:
  - ansible-playbook --vault-password-file vault_password.txt -i inventory/hosts.yml playbooks/server_maintenance.yml
```

## Ansible SSH Key Setup

For Ansible to communicate securely with your servers, you should use SSH keys:

1. **Generate a new SSH key pair**:

   ```bash
   ssh-keygen -t ed25519 -f ~/.ssh/ansible_key
   ```

2. **Place the private key in Ansible Vault**:
   Copy the contents of `~/.ssh/ansible_key` and set it as the value for `ansible_ssh_private_key` in `vault.yml`. Make sure this file is encrypted using Ansible Vault!

3. **Distribute the public key**:
   The public key (found in `~/.ssh/ansible_key.pub`) should be added to the `authorized_keys` file on all target servers. This allows Ansible to authenticate and run commands.

   In your `vault.yml`:

   ```yaml
   ansible_ssh_public_key: "YOUR_PUBLIC_KEY_CONTENT"
   ```

   On target servers, append the public key:

   ```bash
   echo "YOUR_PUBLIC_KEY_CONTENT" >> ~/.ssh/authorized_keys
   ```

By following these steps, Ansible will securely communicate with target servers without requiring a password each time.

# Inventory

The `host.yml` file is our primary inventory file, organizing the various servers into distinct roles and purposes within our infrastructure.

## Global Variables

- `host_key_checking`: Disables SSH key host checking, set to `false` for ease, but be aware of the security implications.
- `ansible_python_interpreter`: Specifies Python3 as the default interpreter.
- `ansible_user` & `ansible_ssh_user`: Default user to SSH as.
- `ansible_become_password`: Sudo password. To keep it secure, this variable should be provided at runtime or encrypted using Ansible Vault.

## Groups

1. **Bootstrap**
   Contains nodes dedicated to bootstrapping the cluster.

   Example:

   ```yaml
   bootstrap:
     hosts:
       master_node_boot:
         ansible_host: 10.10.4.60
   ```

2. **Master Nodes**
   Lists the master nodes in the K3s cluster.

   Example:

   ```yaml
   master_nodes:
     hosts:
       master_node_1: { ansible_host: 10.10.4.61 }
       master_node_2: { ansible_host: 10.10.4.62 }
   ```

3. **Worker Nodes**
   This section is currently commented out but can be used for worker nodes in the K3s cluster.

   Example (Uncomment to use):

   ```yaml
   worker_nodes:
     hosts:
       worker_0: { ansible_host: 10.10.4.80 }
       worker_1: { ansible_host: 10.10.4.81 }
   ```

4. **Servers**
   A group for specialized servers with distinct roles.

   Example:

   ```yaml
   servers:
     hosts:
       ops: { ansible_host: 10.10.4.10 }
   ```

### Adding a New Group (Children Section)

To add a new children section, follow the structure:

```yaml
group_name:
  hosts:
    hostname_1: { ansible_host: IP_ADDRESS }
    hostname_2: { ansible_host: IP_ADDRESS }
    ...
```

Replace `group_name` with the desired group name and add your hosts as shown in the example. Ensure each host under this group is uniquely named and has a corresponding IP address assigned using the `ansible_host` key.

# Cluster Configuration: `main-cluster.yml`

The `main-cluster.yml` in `inventory/group_vars/` contains essential configuration details for setting up the main cluster. It's crucial to correctly understand and set these variables as they help guide the roles in configuring your cluster appropriately.

## Variables

### 1. `k3s_version`

Specifies the version of K3s you wish to install on the nodes.

**Current Value**: `v1.28.1-rc2+k3s1`

**Usage**: When using roles related to K3s setup, this version will be preferred. Ensure you specify a stable and tested version compatible with your applications and infrastructure.

```yaml
k3s_version: "v1.28.1-rc2+k3s1"
```

### 2. `bootstrap_node_ip`

The IP address of the bootstrap node in the cluster.

**Current Value**: `10.10.4.60`

**Usage**: Roles that require knowledge of the bootstrap node (for example, joining worker nodes to the cluster) will utilize this IP.

```yaml
bootstrap_node_ip: 10.10.4.60
```

### 3. `kube_vip_ip`

Specifies the virtual IP address that KubeVip will use for the Kubernetes cluster. This allows for high availability and load balancing among master nodes.

**Current Value**: `10.10.4.40`

**Usage**: Roles related to setting up KubeVip or other high availability configurations will use this IP.

```yaml
kube_vip_ip: 10.10.4.40
```

### 4. `kube_vip_interface`

The network interface that KubeVip will bind to.

**Current Value**: `eth0`

**Usage**: This is crucial for KubeVip's setup and ensures that the virtual IP is correctly associated with a physical or virtual network adapter.

```yaml
kube_vip_interface: "eth0"
```

### 5. `kube_vip_sans_names`

An array of Subject Alternative Names (SANs) for KubeVip. These are crucial for certificate generation and validation, ensuring secure communication within the cluster.

**Current Values**:

- `main.clstr.example.com`
- `10.10.4.40`
- `10.10.4.60`
- `main.cluster`

**Usage**: These SANs will be used during the generation of certificates. Ensure they match your cluster's domain names and IP addresses for proper validation.

```yaml
kube_vip_sans_names:
  - main.clstr.example.com
  - 10.10.4.40
  - 10.10.4.60
  - main.cluster
```

## Adjusting Configuration

To customize the cluster setup, adjust the values in `main-cluster.yml` as per your infrastructure and requirements. It's crucial to ensure that these values align with your actual infrastructure setup and domain names.

## Role READMEs

Below are links to the READMEs for each role:

- [K3s Bootstrap](roles/k3s_bootstrap/README.md)
- [K3s Master](roles/k3s_master/README.md)
- [K3s Worker](roles/k3s_worker/README.md)
- [K3s Uninstall Master](roles/k3s_uninstall_master/README.md)
- [Kubevip](roles/kubevip/README.md)
- [Ubuntu Maintenance](roles/ubuntu_maintenance/README.md)
