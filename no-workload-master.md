# Taint
taint the node for no schedule on masters

```sh
kubectl taint nodes <master-node-name> node-role.kubernetes.io/master=:NoSchedule
```


example
```sh
kubectl taint nodes master-node-boot node-role.kubernetes.io/master=:NoSchedule
kubectl taint nodes master-node-1 node-role.kubernetes.io/master=:NoSchedule
kubectl taint nodes master-node-2 node-role.kubernetes.io/master=:NoSchedule
```