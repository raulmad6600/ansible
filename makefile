encrypt:
	ansible-vault encrypt inventory/vault.yml

decrypt:
	ansible-vault decrypt inventory/vault.yml

maintenance:
	ansible-playbook -i inventory/hosts.yml playbooks/server_maintenance.yml

dvs:
	ansible-playbook -i inventory/hosts.yml playbooks/dvs.yml

main-cluster:
	ansible-playbook -i inventory/hosts.yml playbooks/main_cluster.yml

clstr:
	ansible-playbook -i inventory/hosts.yml clusters/clstr.yml

revan:
	ansible-playbook -i inventory/hosts.yml clusters/revan/cluster.yml


vault-test:
	ansible-playbook -i inventory/hosts.yml dcv/test/vault.yml


uninstall_k3s:
	ansible-playbook -i inventory/hosts.yml playbooks/uninstall_k3s_masters.yml
	ansible-playbook -i inventory/hosts.yml playbooks/uninstall_k3s_workers.yml

taint_masters:
	kubectl taint nodes master-node-boot node-role.kubernetes.io/master=:NoSchedule
	kubectl taint nodes master-node-1 node-role.kubernetes.io/master=:NoSchedule
	kubectl taint nodes master-node-2 node-role.kubernetes.io/master=:NoSchedule


sync:
	git pull origin master